package h3ss3.components.eventdispatcher;

/**
 * Event that can be dispatched.
 * 
 * @author Hessé <sylvain.carite@gmail.com>
 *
 */
public interface Event {

}
