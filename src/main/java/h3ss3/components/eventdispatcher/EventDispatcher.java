package h3ss3.components.eventdispatcher;
import java.util.Map;

/**
 * This class must know all subscribers to be able to dispatch events.
 * 
 * @author Hessé <sylvain.carite@gmail.com>
 *
 */
public interface EventDispatcher {

	/**
	 * Register a subscriber in this dispatcher.
	 * @param subscriber
	 * @return
	 * @throws NullPointerException if one of arguments is null
	 */
	EventDispatcher register(EventSubscriber subscriber);

	/**
	 * Associate a subscriber processor to an event.
	 * @param event
	 * @param subscriberProcessor
	 * @return
	 * @throws NullPointerException if one of arguments is null
	 */
	EventDispatcher register(Event event, EventSubscriberProcessor subscriberProcessor);

	/**
	 * Dispatch an event to corresponding subscribers and give them event data.
	 * @param event
	 * @param eventData
	 * @throws NullPointerException if one of arguments is null
	 */
	void dispatch(Event event, Map<String, Object> eventData);

}