package h3ss3.components.eventdispatcher;
import java.util.Map;

/**
 * This class associate subscriber processors to events.
 * 
 * @author Hessé <sylvain.carite@gmail.com>
 *
 */
public interface EventSubscriber {

	/**
	 * Return a map of subscriber processors associated to events.
	 * @return
	 */
	Map<Event, EventSubscriberProcessor> getSubscriptions();
}
