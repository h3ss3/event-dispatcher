package h3ss3.components.eventdispatcher;
import java.util.Map;

/**
 * Processing corresponding to an event.
 * 
 * @author Hessé <sylvain.carite@gmail.com>
 *
 */
public interface EventSubscriberProcessor {

	/**
	 * Process an event.
	 * @param event
	 * @param eventData
	 * @throws NullPointerException if one of arguments is null
	 * @throws EventSubscriberProcessorException an instance of a child class will be thrown if something went wrong
	 */
	void process(Event event, Map<String, Object> eventData) throws EventSubscriberProcessorException;
}
