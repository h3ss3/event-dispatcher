package h3ss3.components.eventdispatcher;

public class EventSubscriberProcessorException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8375049348360250977L;

	public EventSubscriberProcessorException(Throwable cause) {
		super(cause);
	}
}
