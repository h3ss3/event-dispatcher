package h3ss3.components.eventdispatcher.impl;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import h3ss3.components.eventdispatcher.Event;
import h3ss3.components.eventdispatcher.EventDispatcher;
import h3ss3.components.eventdispatcher.EventSubscriber;
import h3ss3.components.eventdispatcher.EventSubscriberProcessor;
import h3ss3.components.eventdispatcher.EventSubscriberProcessorException;
import h3ss3.components.eventdispatcher.utils.ConcurrentHashMapWrapper;

/**
 * Basic implementation of EventDispatcher.
 * 
 * @author Hessé <sylvain.carite@gmail.com>
 *
 * @ThreadSafe
 */
public class SimpleEventDisptacher implements EventDispatcher {

	private static SimpleEventDisptacher instance = new SimpleEventDisptacher();
	
	private List<EventSubscriber> subscribers;
	
	public static synchronized SimpleEventDisptacher getInstance() {
		return instance;
	}
	
	private SimpleEventDisptacher()
	{
		subscribers = new Vector<>();
	}

	@Override
	public synchronized SimpleEventDisptacher register(EventSubscriber subscriber)
	{
		if (subscriber == null) {
			throw new NullPointerException("subscriber cannot be null");
		}
		
		subscribers.add(subscriber);
		
		return this;
	}

	@Override
	public synchronized SimpleEventDisptacher register(Event event, EventSubscriberProcessor subscriberProcessor)
	{
		if (event == null) {
			throw new NullPointerException("event cannot be null");
		}
		if (subscriberProcessor == null) {
			throw new NullPointerException("subscriberProcessor cannot be null");
		}
		
		Map<Event, EventSubscriberProcessor> subscriptions = 
				new ConcurrentHashMapWrapper<Event, EventSubscriberProcessor>()
						.put(event, subscriberProcessor)
						.toConcurrentHashMap();
		
		subscribers.add(() -> subscriptions);
		
		return this;
	}
	
	@Override
	public void dispatch(Event event, Map<String, Object> eventData)
	{
		if (event == null) {
			throw new NullPointerException("event cannot be null");
		}
		if (eventData == null) {
			throw new NullPointerException("eventData cannot be null");
		}
		
		for(EventSubscriber subscriber : subscribers) {
			if (subscriber.getSubscriptions().containsKey(event)) {
				try {
					subscriber.getSubscriptions().get(event).process(event, eventData);
				} catch (EventSubscriberProcessorException e) {
					//TODO log with event + eventData
					System.out.println("Event "+event.toString()+" cannot be processed with data "+eventData.toString());
				}
			}
		}
	}
}
