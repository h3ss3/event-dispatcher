package h3ss3.components.eventdispatcher.impl;
import java.util.Map;

import h3ss3.components.eventdispatcher.Event;
import h3ss3.components.eventdispatcher.EventDispatcher;
import h3ss3.components.eventdispatcher.EventSubscriber;
import h3ss3.components.eventdispatcher.EventSubscriberProcessor;
import h3ss3.components.eventdispatcher.EventSubscriberProcessorException;
import h3ss3.components.eventdispatcher.utils.ConcurrentHashMapListWrapper;
import h3ss3.components.eventdispatcher.utils.ConcurrentHashMapWrapper;

/**
 * Optimized implementation of EventDispatcher.
 * 
 * @author Hessé <sylvain.carite@gmail.com>
 *
 * @ThreadSafe
 */
public class OptimizedEventDispatcher implements EventDispatcher {

	private static OptimizedEventDispatcher instance = new OptimizedEventDispatcher();
	
	private ConcurrentHashMapListWrapper<Event, EventSubscriber> subscribers;
	
	public static synchronized OptimizedEventDispatcher getInstance() {
		return instance;
	}
	
	private OptimizedEventDispatcher()
	{
		subscribers = new ConcurrentHashMapListWrapper<>();
	}

	@Override
	public OptimizedEventDispatcher register(EventSubscriber subscriber)
	{
		if (subscriber == null) {
			throw new NullPointerException("subscriber cannot be null");
		}
		
		for(Event event : subscriber.getSubscriptions().keySet()) {
			subscribers.addValue(event, subscriber);
		}
		
		return this;
	}

	@Override
	public OptimizedEventDispatcher register(Event event, EventSubscriberProcessor subscriberProcessor)
	{
		if (event == null) {
			throw new NullPointerException("event cannot be null");
		}
		if (subscriberProcessor == null) {
			throw new NullPointerException("subscriberProcessor cannot be null");
		}
		
		Map<Event, EventSubscriberProcessor> subscriptions = 
				new ConcurrentHashMapWrapper<Event, EventSubscriberProcessor>()
						.put(event, subscriberProcessor)
						.toConcurrentHashMap();
		
		subscribers.addValue(event, () -> subscriptions);
		
		return this;
	}
	
	@Override
	public void dispatch(Event event, Map<String, Object> eventData)
	{
		if (event == null) {
			throw new NullPointerException("event cannot be null");
		}
		if (eventData == null) {
			throw new NullPointerException("eventData cannot be null");
		}
		
		for(EventSubscriber subscriber : subscribers.get(event)) {
			try {
				subscriber.getSubscriptions().get(event).process(event, eventData);
			} catch (EventSubscriberProcessorException e) {
				//TODO log with event + eventData
				System.out.println("Event "+event.toString()+" cannot be processed with data "+eventData.toString());
			}
		}
	}
}
