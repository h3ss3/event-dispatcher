package h3ss3.components.eventdispatcher.utils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * ConcurrentHashMap<K, V> with some additional facilities.
 * The main facility is the capability to chain calls (using "return this" in methods).
 * 
 * @author Hessé <sylvain.carite@gmail.com>
 *
 * @param <K>
 * @param <V>
 * 
 * @ThreadSafe
 */
public class ConcurrentHashMapWrapper<K, V> {
	
	private Map<K, V> map;

	public ConcurrentHashMapWrapper()
	{
		map = new ConcurrentHashMap<>();
	}
	
	/**
	 * Set a value.
	 * {@link java.util.concurrent.ConcurrentHashMap#put(Object, Object)}
	 * @param key
	 * @param value
	 * @return
	 */
	public ConcurrentHashMapWrapper<K, V> put(K key, V value)
	{
		map.put(key, value);
		
		return this;
	}
	
	/**
	 * Check if a key exists.
	 * {@link java.util.concurrent.ConcurrentHashMap#containsKey(Object)}
	 * @param key
	 * @return
	 */
	public boolean containsKey(K key)
	{
		return map.containsKey(key);
	}
	
	/**
	 * Get a value given its key.
	 * {@link java.util.concurrent.ConcurrentHashMap#get(Object)}
	 * @param key
	 * @return
	 */
	public V get(K key)
	{
		return map.get(key);
	}
	
	/**
	 * Return the corresponding ConcurrentHashMap.
	 * @return
	 */
	public Map<K, V> toConcurrentHashMap()
	{
		return map;
	}
}
