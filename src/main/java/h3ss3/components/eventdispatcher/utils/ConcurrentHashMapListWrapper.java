package h3ss3.components.eventdispatcher.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * ConcurrentHashMap<K, List<V>> with some additional facilities.
 * 
 * @author Hessé <sylvain.carite@gmail.com>
 *
 * @param <K>
 * @param <V>
 * 
 * @ThreadSafe
 */
public class ConcurrentHashMapListWrapper<K, V> extends ConcurrentHashMapWrapper<K, List<V>> {

	/**
	 * Add a subvalue in value given its key.
	 * @param key
	 * @param value
	 * @return
	 */
	public synchronized ConcurrentHashMapListWrapper<K, V> addValue(K key, V value)
	{
		List<V> values = get(key);
		if (values == null) {
			values = put(key, new ArrayList<>()).get(key);
		}
		values.add(value);
		
		return this;
	}
}
