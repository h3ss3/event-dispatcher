# Event dispatcher

## Description

This library implements the pattern producer/subscriber in a synchronous way.

## Install with maven

Add following dependency:
```
<dependency>
    <groupId>h3ss3.components</groupId>
    <artifactId>event-dispatcher</artifactId>
    <version>1.0.0-SNAPSHOT</version>
</dependency>
```

## Key points

`Event` is the interface of an event.
`EventSubscriberProcessor` is a task executed when event will be dispatched.
`EventSubscriber` makes one-to-one association between events and `EventSubscriberProcessor`.
`EventDispatcher` dispatch the event, i.e it sequentially launches every `EventSubscriberProcessor` associated to the dispatched event.

## Usage

1. Create an enum implementing `h3ss3.components.eventdispatcher.Event` with one enum item for one event.
2. Create subscribers implementing `h3ss3.components.eventdispatcher.EventSubscriber`.
3. Choose an implementation of `h3ss3.components.eventdispatcher.EventDispatcher` among `h3ss3.components.eventdispatcher.impl.*`.
4. Create a loader that will use the chosen event dispatcher to register subscribers.
